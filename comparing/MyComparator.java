package comparing;

import java.util.Arrays;
import java.util.Comparator;

public class MyComparator implements Comparator<String> {
    public int compare(String a, String b) {
        return b.toLowerCase().compareTo(a.toLowerCase());
    }

    public static void main(String[] args) {
        Comparator<String> comparator = (a, b) -> b.compareTo(a);

        String[] values = {"123", "Abb", "aab"};
        Arrays.sort(values, new MyComparator());
        for (var s : values) {
            System.out.print(s + " ");
        }

        System.out.println();

        String[] valuesBis = {"123", "Abb", "aab"};
        Arrays.sort(valuesBis, comparator);
        for (var s : valuesBis) {
            System.out.print(s + " ");
        }
    }
}
