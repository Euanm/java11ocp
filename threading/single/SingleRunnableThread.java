package threading.single;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SingleRunnableThread {

    public static void main(String... args) {
        Runnable taskOne = () -> {
            System.out.println("Println something..");
        };
        Runnable taskTwo = () -> {
            for(int i = 0; i<5; i++) {
                System.out.println("Printing item: "+i);
            }
        };
        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            System.out.println("Start");
            service.execute(taskOne);
            service.execute(taskTwo);
            service.submit(taskOne); // submit method ok with Runnable tasks
            System.out.println("End");
        } finally {
            if(service != null) service.shutdown();
        }

        try {
            if(service != null) {
                service.awaitTermination(1, TimeUnit.SECONDS); // adds a Timeout, waiting all tasks to finish
            }
        } catch (InterruptedException ex) {
            System.out.println("interrupted");
        }
    }
}
