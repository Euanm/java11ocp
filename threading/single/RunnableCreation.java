package threading.single;

public class RunnableCreation {

    public static void main(String[] args) {
        // printing asynchronously
        System.out.println("Starting");
        new ReadThread().start();
        new Thread(new PrintData()).start();
        new ReadThread().start();
        System.out.println("End");
    }
}

class PrintData implements Runnable {
    @Override
    public void run() {
        for(int i = 0; i<3; i++) {
            System.out.println("Printing record: "+ i);
        }
    }
}

class ReadThread extends Thread {
    @Override
    public void run() {
        System.out.println("Reading a resource");
    }
}
