package threading.single;

import java.util.concurrent.*;

public class SingleScheduledThread {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ScheduledExecutorService service =
                Executors.newSingleThreadScheduledExecutor();
        Runnable runnable = () -> System.out.print("hello ");
        Callable<String> callable = () -> "Euan";
        // schedule runnable
        service.schedule(runnable, 3, TimeUnit.SECONDS);
        // schedule callable
        ScheduledFuture<?> result = service.schedule(callable, 7, TimeUnit.SECONDS);
        System.out.println(result.get());

        if(service!=null) service.shutdown();
    }
}
