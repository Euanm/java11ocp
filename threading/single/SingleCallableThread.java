package threading.single;

import java.util.concurrent.*;

public class SingleCallableThread {

    public static void main(String... args) throws InterruptedException, ExecutionException {
        ExecutorService service = Executors.newSingleThreadExecutor();
        try {
            Callable<Integer> callable = () -> 50 + 27;
            Future<Integer> future = service.submit(callable);
            System.out.println(future.get(10, TimeUnit.SECONDS)); // get results with a timeout
        } catch (TimeoutException ex) {
            System.out.println("Time is out !");
        } finally {
            if (service != null) service.shutdown();
        }
    }
}
