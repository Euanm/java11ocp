package threading.single;

import java.util.List;
import java.util.concurrent.*;

public class InvokeSingleThread {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService service = Executors.newSingleThreadExecutor();
        Callable<String> callable1 = () -> "output 1";
        Callable<String> callable2 = () -> "output 2";
        Callable<String> callable3 = () -> "output 3";
        System.out.println("Start");
        // invokeAll waits for all tasks to complete
        List<Callable<String>> callables = List.of(callable1, callable2, callable3);
        List<Future<String>> futureList = service.invokeAll(callables);
        for(Future<String> future : futureList) {
            System.out.println(future.get());
        }
        // invokeAny waits for first task to complete
        String result = service.invokeAny(callables);
        System.out.println(result);
        System.out.println("End");

        if(service!=null) service.shutdown();
    }
}
