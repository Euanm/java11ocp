package threading.single;

public class Polling {
    private static int counter = 0;

    public static void main(String... args)  throws InterruptedException {
        new Thread(() -> {
           for(int i = 0; i<500 ; i++) {
               counter++;
           }
        }).start();
        while(counter < 300) {
            System.out.println("Not reached yet");
            Thread.sleep(500);
        }
        System.out.println("Goal !");
    }
}
