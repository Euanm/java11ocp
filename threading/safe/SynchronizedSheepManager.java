package threading.safe;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SynchronizedSheepManager {
    private int sheepCount = 0;
    private final Object myLock = new Object(); // final to protect from reassigning
    private void incrementAndReport() {
        synchronized(myLock) { // Any Object used by all the thread (could also be 'this', for example)
            System.out.print(++sheepCount+ " ");
        }
    }

    private synchronized void incrementAndReportSync() {
        System.out.print(++sheepCount+ " ");
    }

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(20);
        try {
            SynchronizedSheepManager manager = new SynchronizedSheepManager();
            // Using synchronized block
            for (int i = 0; i < 10; i++) {
                service.submit(manager::incrementAndReport);
            }
            // using synchronized method
            for (int i = 0; i < 10; i++) {
                service.submit(manager::incrementAndReportSync);
            }
        } finally {
            service.shutdown();
        }
    }
}
