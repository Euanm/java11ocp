package threading.safe;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockSheepManager {
    private int sheepCount = 0;
    private final Lock myLock = new ReentrantLock();
    private void incrementAndReport() {
        try {
            myLock.lock();
            System.out.print(++sheepCount+ " ");
        } finally {
            myLock.unlock();
        }
    }

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(20);
        try {
            LockSheepManager manager = new LockSheepManager();
            // Using lock
            for (int i = 0; i < 10; i++) {
                service.submit(manager::incrementAndReport);
            }
        } finally {
            service.shutdown();
        }
    }
}
