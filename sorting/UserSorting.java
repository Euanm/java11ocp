package sorting;

import java.util.Collections;
import java.util.List;
import java.util.Arrays;
import java.util.Comparator;

public class UserSorting implements Comparable<UserSorting> {

    private String name;
    private int total;

    public static void main(String... args) {
        UserSorting user1 = new UserSorting("Paula", 3);
        UserSorting user2 = new UserSorting("Peter", 5);
        UserSorting user3 = new UserSorting("Peter", 7);

        List<UserSorting> theList = Arrays.asList(user1, user2, user3);

        // sort 1: sort by name
        Collections.sort(theList, Comparator.comparing(UserSorting::getName));

        System.out.println(theList);

        // sort 2: sort by name, then by total
        Collections.sort(theList, Comparator.comparing(UserSorting::getName)
                .thenComparingInt(UserSorting::getTotal));

        System.out.println(theList);

        // sort 3: sort by name, then by total, then reverse
        Collections.sort(theList, Comparator.comparing(UserSorting::getName)
                .thenComparingInt(UserSorting::getTotal).reversed());

        System.out.println(theList);

        // sort 4
        Collections.sort(theList, Comparator.reverseOrder());

        System.out.println(theList);
    }

    public UserSorting(String name, int total) {
        this.name = name;
        this.total = total;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String toString() {
        return "" + total;
    }

    @Override
    public int compareTo(UserSorting o) {
        return this.getTotal() - o.getTotal();
    }
}
