package collectors;

import java.util.function.Supplier;

@FunctionalInterface
public interface CollectorBuilder<T> {

    T build(Supplier<T> supplier);
}
