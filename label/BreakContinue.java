package label;

public class BreakContinue {

    public static void main (String[] args) {
        OUTER: for (int i = 0; i < 4 ; i++) {
            INNER: for (int j = 0; j < 4 ; j++) {
                if( i == j) {
                    break INNER;
                }
                System.out.print("(" + i +","+ j +") ");
            }
            System.out.println();
        }
    }
}
