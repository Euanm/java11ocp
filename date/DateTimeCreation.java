package date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;

public class DateTimeCreation {

    public static void main(String... args) {
        // Date creation
        LocalDate date = LocalDate.of(2022, Month.JULY, 01); // year, month, day

        // Time creation
        LocalTime time = LocalTime.of(11, 38, 45); // hour, minutes, seconds

        // DateTime creation
        LocalDateTime datetime = LocalDateTime.of(date, time); // local date, local time

        // year, month, day, hour, minutes, seconds
        LocalDateTime dateTime2 = LocalDateTime.of(2021, Month.JUNE, 22, 13, 45, 32);
    }
}
