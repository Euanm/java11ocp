package exceptions.closable;

import java.io.Closeable; // AutoCloseable in java.lang

public class MyRes implements Closeable {

    String name;

    public MyRes(String name) {
        this.name = name;
    }

    public void close() {
        System.out.println("closed");
    }
}
