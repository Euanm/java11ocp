package exceptions.closable;

public class RunTest {

    public static void main(String... args) {
        try(MyRes myRes = new MyRes("Euan")) {
            System.out.println("trying");
        } finally {
            System.out.println("finalled");
        }
    }
}
