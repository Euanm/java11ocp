package exceptions.finalement;

public class Finalement {

    public static void main (String... args) throws StackOverflowError {
        try {
            System.out.println("A");
            throw new Error();
        } catch (Error e) {
            System.out.println("B");
            throw new NumberFormatException();
        } catch (ArithmeticException e) {
        } catch (RuntimeException exc) {
        } finally {
            System.out.println("C"); // exécuté avant de sortir
            throw new ArithmeticException(); // Cette exception écrase la précédente
        }
        // System.out.println("D"); unreachable
    }
}
