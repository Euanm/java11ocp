package exceptions.finalement;

public class Retourne {

    private String retourne() {
        try {
            String val = "toto";
            return val;
        } finally {
            return "tata";
        }
    }

    public static void main (String[] args) {
        Retourne retourne = new Retourne();
        String ret = retourne.retourne();

        System.out.println(ret);
    }
}
