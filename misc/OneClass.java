package misc;

import java.util.ArrayList;
import java.util.List;

@Deprecated(since="3.1.2", forRemoval = true)
public class OneClass {

    @Deprecated
    void aFct( @Deprecated String arg) {
        @Deprecated int nb;
    }

    int size() {
        List list = new ArrayList();
        return list.size();
    }

    public static void main(String... args) {
        double db = 12;

        String str = "hello: " + db;
        System.out.println(str);

        OneClass oneClass = new OneClass();
        oneClass.size();
        oneClass.aFct("arg");
    }
}
