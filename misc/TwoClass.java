package misc;

import java.util.ArrayList;

@SuppressWarnings({"deprecation", "unchecked"})
public class TwoClass {

    void myFct() {
        OneClass oneClass = new OneClass();
    }

    void anotherFct() {
        ArrayList list = new ArrayList();
    }
}
