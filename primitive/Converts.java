package primitive;

public class Converts {
    // short
    short s = 2;
    short sh = -4;
    // float
    float f = 1;
    float fl = -54;

    public static void main(String[] args) {
        short verylong = (short) 4444444;
        System.out.println(verylong); // prints -12004

        char myChar = 'a'; // unsigned
        //
        byte myByte = 'a';
        myChar = (char) myByte;

        System.out.println("myChar: "+ myChar);
        myChar = 98; // implicit cast (char) 98
        System.out.println("myChar: "+ myChar);
        int myInt = 99;
        myChar = (char) myInt;
        System.out.println("myChar: "+ myChar);
        myInt = myChar; // no cast
        System.out.println("myInt: "+ myInt);

        System.out.println("myByte: "+ myByte); // displays numeric value of char
        byte myByte2 = (byte) myChar;
        System.out.println("myByte2: "+ myByte2);
        short myShort = (short) myChar;
        System.out.println("myShort: "+ myShort);

        myShort = 127;
        // myChar = myShort; KO
    }
}
