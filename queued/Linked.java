package queued;

import java.util.LinkedList;

public class Linked {

    public static void main(String[] args) {
        var greetings = new LinkedList<String>();
        greetings.add("Hello");
        greetings.offer("Hi");
        greetings.offer("Hallo");
        System.out.println(greetings.peek());
        System.out.println(greetings.element());
        System.out.println(greetings);
        System.out.println(greetings.poll());
        System.out.println(greetings);
        System.out.println(greetings.remove());
        System.out.println(greetings);
        System.out.println(greetings.pop()); // pop(), remove() alias pour removeFirst()
        System.out.println(greetings);
        System.out.println(greetings.pop()); // throws NoSuchElementException
    }
}
