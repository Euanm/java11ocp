package heritage;

@InheritedOne
public class MyClass {
    static void foo()  {}

    @Deprecated
    private void bar() {
        System.out.println("Parent bar");
    }

    void regular() {}
}
