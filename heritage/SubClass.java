package heritage;

import java.util.*;

public class SubClass extends @TypeAction MyClass { // extends

    @TypeAction // constructor
    SubClass() {}

    // @Override KO on static method - Hidden - not inherited
    static void foo() { }

    // @Override KO parent method is private
    public void bar() {
        System.out.println("sub bar");
    }

    @Override // overriding OK
    public void regular() {
        System.out.println("This is the regular method");
    }

    @TypeAction // method (no void return)
    public SubClass getInstance(@TypeAction SubClass sub) { // Parameter
        return new SubClass();
    }

    public static void main(String[] args) {
        MyClass mc1 = new @TypeAction SubClass(); // new
        SubClass s1 = new SubClass();
        MyClass mc2 = s1;
        s1 = (@TypeAction SubClass) mc2; // cast
        //mc1.bar();
        s1.bar();

        Set set = new TreeSet();
        //set.add(1);
        set.add("3");
        set.add("4");

        System.out.println(set);

    }
}
