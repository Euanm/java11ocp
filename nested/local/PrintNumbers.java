package nested.local;

public class PrintNumbers {
    private int length = 5;

    private int x = 12;

    public void calculate() {
        int width = 20;  // width is effectively final

        class MyLocalClass {
            private static final int goose = 4;  // final needed

            private int x = 24;

            public void multiply() {
                System.out.println("=== inside multiply() ===");
                // local variable 'width' needs to be final or effectively final
                System.out.println(length * width); // enclosing class private field access
                anotherFunc(); // enclosing class private method access
                System.out.println(PrintNumbers.this.x); // outer class private field
                System.out.println(this.x); // local class private field
                System.out.println(x); // local class private field
            }
        }

        // width = 22;

        var myLocalClass = new MyLocalClass();
        myLocalClass.multiply();
        System.out.println("=== inside calculate() ===");
        System.out.println(myLocalClass.goose); // static final inner member
        System.out.println(myLocalClass.x); // inner member
    }

    private void anotherFunc() {
        System.out.println("hello");
    }

    public static void main(String[] args) {
        PrintNumbers outer = new PrintNumbers();
        outer.calculate();
    }
}
