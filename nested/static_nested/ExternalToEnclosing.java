package nested.static_nested;

public class ExternalToEnclosing {

    public static void main(String[] args) {
        Enclosing.StaticNested staticNested = new Enclosing.StaticNested();
        // public inner
        Enclosing.StaticNested.PublicInner publicInner = staticNested.new PublicInner();

        // private inner
        // Enclosing.StaticNested.PrivateInner privateInner;
        // KO PrivateInner class has private access
    }
}
