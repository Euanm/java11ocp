package nested.static_nested;

public class Enclosing {

    private int hello = 5;
    private String str = "hello";

    /**
     * Not static: has to be used with an instance of the outer class
     */
    public class PublicNested {
        private int price = 6;
        private static final int toto = 12; // final needed
        private void myFunc() {
            System.out.println(hello);
        }
        // private static void myStaticFunct() { System.out.println("static"); } => static method KO pour inner class
        private void fct() { System.out.println(str); } // outer member access ok without instance ref (implicit this)
    }

    /**
     * Static: can be instanciated without an instance of the enclosing class
     */
    static class StaticNested {
        private int price = 4;
        private static int cpt = 31; // no final keyword needed
        private void myFunc() {
            System.out.println(price);
        }
        private static void myStaticFunct() { System.out.println("static"); } // static method OK pour inner static class
        private void fct() { System.out.println(new Enclosing().str); }  // outer member access needs outer instance ref

        private class PrivateInner {
            private String getMySecret()  {
                return "I love you !";
            }
        }

        public class PublicInner {
            public String getPublicAnnoucement() {
                return "Hello everybody !";
            }
        }
    }

    private void fct() {
        new StaticNested();
    }

    private static void staticFct() {
        new StaticNested(); // No need of outer instance
    }

    public static void main(String[] args) {
        // Inner Class
        System.out.println("=== Inner class ===");
        Enclosing enclosing = new Enclosing();
        PublicNested nested = enclosing.new PublicNested();
        System.out.println(nested.price);
        System.out.println(PublicNested.toto);
        nested.myFunc(); // inner private method access from outer

        // Static inner/nested class
        System.out.println("=== Static nested class ===");
        StaticNested staticNested = new StaticNested();
        System.out.println(staticNested.price);
        System.out.println(staticNested.cpt);
        staticNested.myFunc(); // inner private method access from outer

        // Calling a private inner class
        System.out.println("=== Private nested class ===");
        Enclosing.StaticNested.PrivateInner privateInner = staticNested.new PrivateInner();
        System.out.println(privateInner.getMySecret());
    }
}
