package nested.inner;

public class OuterClass {

    private int cpt = 5;

    private MyInnerClass cstInner() {
        return new MyInnerClass();
    }

    private void outerFct() {
        System.out.println("Calling an outer method");
    }

    public abstract class InClass {}

    public final class MyInnerClass extends InClass {
        void printCpt() {
            // outer class member usage
            System.out.println("outer cpt: "+ cpt); // implicit qualifier to outer class
            for(int cpt = 0; cpt<3; cpt++) {
                System.out.println("inner cpt: "+ cpt);
            }
            outerFct(); // appel fct outer
        }
    }

    public void regularFct() {
        new MyInnerClass(); // OK instance = this
    }

    public static void staticFct() {
        // new MyInnerClass(); KO needs an instance of outer class
    }

    public final static class MyInnerStaticClass {}

    public static void main(String[] args) {
        OuterClass outerClass = new OuterClass();
        InClass inClass = outerClass.new MyInnerClass();
        MyInnerClass myInnerClass = (MyInnerClass) inClass;
        myInnerClass.printCpt();

        new MyInnerStaticClass();
    }
}
