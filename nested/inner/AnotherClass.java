package nested.inner;

public class AnotherClass {

    public void main(String[] args) {
        OuterClass innerClasses = new OuterClass();
        OuterClass.InClass inClass = innerClasses.new MyInnerClass();

        OuterClass.MyInnerStaticClass innerStatic = new OuterClass.MyInnerStaticClass();
    }
}
