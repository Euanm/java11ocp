package nested.anonymous;

public class ZooGiftShop {
    abstract class SaleTodayOnly {
        abstract int dollarsOffToday();
    }

    interface SaleThisWeekOnly {
        int dollarsOffthisWeek();
    }

    public int admission (int basePrice) {
        SaleTodayOnly saleTodayOnly = new SaleTodayOnly() {
            int dollarsOffToday() { return 3; } // public not needed
        };

        SaleThisWeekOnly saleThisWeekOnly = new SaleThisWeekOnly() {

            public static final int nb = 10; // static uniquement si final
            public int dollarsOffthisWeek() { return 1; } // interfaces require public methods
        };

        return basePrice - saleThisWeekOnly.dollarsOffthisWeek() - saleTodayOnly.dollarsOffToday();
    }
}
