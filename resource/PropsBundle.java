package resource;

import java.util.Locale;
import java.util.ResourceBundle;

public class PropsBundle {

    public static void main(String... args) {
        ResourceBundle bundle = ResourceBundle.getBundle("toto", Locale.US);

        System.out.println(bundle.getString("name")+" "+bundle.getString("profession"));
    }
}
