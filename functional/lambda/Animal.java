package functional.lambda;


public class Animal {
    private String species;
    private boolean canHop;
    private boolean canSwimm;

    public Animal(String species, boolean hopper, boolean swimmer) {
        this.species = species;
        canHop = hopper;
        canSwimm = swimmer;
    }

    public boolean canHop() { return canHop; }
    public boolean canSwimm() { return canSwimm; }
    public String toString() { return species; }
}
