package functional.lambda;

@FunctionalInterface // Regular functional interface
public interface FctInterface {

    public boolean test();

    default void def() {System.out.println("This is a default method");}

    private boolean priv() { return Boolean.FALSE;  }
}
