package streams;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class HowToCollect {

    public static void main(String[] args) {
        List<String> list = List.of("a", "b", "c", "1", "2", "3");
        Supplier<Collector> supplier = Collectors::toSet;
        list.stream().collect(supplier.get());
    }
}
