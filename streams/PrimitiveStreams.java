package streams;

import java.util.Arrays;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.OptionalInt;

public class PrimitiveStreams {

    public static void main (String[] args) {
        Stream<Integer> stream = Stream.of(2);
        IntStream insStream = IntStream.of(3);
        // From int array
        int[] ints = {2, 7, 9, 5, 6, 5, 8, 3};
        IntPredicate intPredicate = i -> i % 2 == 1;
        int sum = Arrays.stream(ints)
                .distinct() // doublons
                .sorted() // tri
                .filter(intPredicate) // filtre
                .peek(System.out::println) // log eléments
                .sum(); // Opération Terminaison pour int
        System.out.println("sum: "+ sum);

        OptionalInt res = Arrays.stream(ints)
                .distinct()
                .filter(intPredicate)
                .reduce( (a, b) -> a + b);
        System.out.println("reduced: "+ res);

        // IntStream range
        // No trigger without Terminal Operation
        IntPredicate pred = i -> i % 2 == 1;
        IntStream ranged = IntStream.rangeClosed(1, 10)
                .filter(pred) // filtre
                .peek(System.out::println); // log éléments

        // summaryStatistics Terminal Operation
        var stats = ranged.summaryStatistics();
        System.out.println("*** Stats ***");
        System.out.println(stats);
    }
}
