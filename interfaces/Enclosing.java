package interfaces;

public class Enclosing {

    interface Interf {
        void myFct();
        private int rest() { myFct(); return 2;};
        default void def() { myFct(); }
    }

    static class InnerStatique implements Interf {
        public void myFct() {}
    }

    class NonStatic {}

    static void myFct() {
        // Enclosing.InnerStatique inner = Enclosing.new InnerStatique(); => KO
        Enclosing.InnerStatique inner = new Enclosing.InnerStatique();
    }
}
