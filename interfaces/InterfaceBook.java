package interfaces;

public interface InterfaceBook {
    int amount = 10;
    static String str = "hello"; // always public

    enum Choice {
        one, two , three
    }

    default void defaultFct() { // always public
        privateFct();
        System.out.println(str);
    }

    private void privateFct() {
        System.out.println(str);
    }

    static void staticFct() {
        System.out.println(str);
    }

    private static void privateStaticFct() {
        System.out.println(str);
    }
}
