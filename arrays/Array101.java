package arrays;

import java.util.Arrays;

public class Array101 {

    public static void main(String[] args) {
        int[] unsorted = {3, 2, 9, 6, 4};
        int [] sorted = Arrays.copyOf(unsorted, 5);
        Arrays.sort(sorted);
        System.out.println("Unsorted");
        System.out.println(Arrays.toString(unsorted));
        System.out.println("Sorted");
        System.out.println(Arrays.toString(sorted));
    }
}
