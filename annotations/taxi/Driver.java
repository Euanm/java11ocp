package annotations.taxi;

public @interface Driver {
    int[] directions();
    String name() default "";
}
