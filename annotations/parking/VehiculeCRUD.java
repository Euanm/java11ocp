package annotations.parking;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.CLASS)
@Documented
public @interface VehiculeCRUD {
    String type();
}
