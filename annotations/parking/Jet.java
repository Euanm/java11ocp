package annotations.parking;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Documented
@Retention(RetentionPolicy.SOURCE)
public @interface Jet {
    String value() default "bireacteur";
}
