package annotations.parking;

@VehiculeGroup
public class Parking {

    private Vehicule train;

    @Plane(rangeDist=2000, model="777", engine= Plane.Engine.ROLLS_ROYCE)
    @Clients({"Air Quatar", "Easy Jet"})
    @AircraftVersion(version="777-EX")
    @AircraftVersion(version="777-short")
    @AircraftVersion
    @AircraftVersion
    private Vehicule boeing;

    @Plane(rangeDist=1800, seatsNumber=150, model="A320", engine= Plane.Engine.SNECMA)
    @Clients(value={"Air France", "Ryanair"})
    @Jet("biréacteur")
    @AircraftVersion(version="A320-NEO")
    @AircraftVersion(version="A320-court")
    @AircraftVersion(version="A320-long")
    private Vehicule airbus;

    @Plane(rangeDist=1700, model="E200", engine = Plane.Engine.ROLLS_ROYCE)
    @Clients(value="Air Brasil")
    private Vehicule embraer;

    // method + parameter

    /**
     *
     * @param vehicule
     */
    @VehiculeCRUD(type="Adding")
    @Methodic
    void addJet(@Jet Vehicule vehicule) { }

    // Constructor
    @Creating
    Parking() {}

    // @Methodic Method @Target type not applicable to constructor
    Parking(Vehicule vehicule) {
        this.train =  vehicule;
    }

    void aFct(Vehicule vehicule) throws Exception {}
}
