package annotations.parking;

import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.SOURCE)
@Repeatable(AircraftVersions.class)
public @interface AircraftVersion {
    String version() default "A";
}
