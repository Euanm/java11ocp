package annotations.parking;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.CLASS)
public @interface Plane {

    int constante = 12; // implicit

    int rangeDist();

    int seatsNumber() default 200;

    String model() default "-";

    Engine engine();

    enum Engine {
        SNECMA, ROLLS_ROYCE, SAAB
    }
}
