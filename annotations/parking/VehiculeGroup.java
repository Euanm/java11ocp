package annotations.parking;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target(ElementType.TYPE)
@Ensemble
@Documented
public @interface VehiculeGroup {
}
