package formatting;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class DateFormatting {

    public static void main(String[] args) {
        // local date
        System.out.println("*** Date formatting ***");
        LocalDate date = LocalDate.now();
        System.out.println(date.format(DateTimeFormatter.ISO_LOCAL_DATE));
        System.out.println(date.getDayOfWeek());
        System.out.println(date.getDayOfYear());

        // local time
        System.out.println("*** Time formatting ***");
        LocalTime time = LocalTime.now();
        System.out.println(time.format(DateTimeFormatter.ISO_LOCAL_TIME));

        // Local date time
        System.out.println("*** Date time formatting ***");
        LocalDateTime dateTime = LocalDateTime.now();
        System.out.println(dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));

        // from pattern
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy 'at' hh:mm:ss");
        System.out.println(formatter.format(dateTime));
        // OR
        System.out.println(dateTime.format(formatter));

    }
}
