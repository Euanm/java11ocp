package formatting;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class LocalizedDate {

    public static void main(String[] args) {
        Locale.setDefault(Locale.CHINESE);
        LocalDateTime dateTime = LocalDateTime.now();

        System.out.println("*** Format date ***");
        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL);
        System.out.println(formatter.format(dateTime));
        formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG);
        System.out.println(formatter.format(dateTime));
        formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
        System.out.println(formatter.format(dateTime));
        formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);
        System.out.println(formatter.format(dateTime));

        System.out.println("*** Format time ***");
        ZonedDateTime zoned = ZonedDateTime.now();
        formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.FULL).withLocale(Locale.US);
        System.out.println(formatter.format(zoned)); // needs zoned date time
        formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.LONG);
        System.out.println(formatter.format(zoned)); // needs zoned date time
        formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.MEDIUM);
        System.out.println(formatter.format(dateTime));
        formatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT);
        System.out.println(formatter.format(dateTime));

        System.out.println("*** Format date time ***");
        Locale.setDefault(Locale.TAIWAN);
        formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL).withLocale(Locale.FRANCE);
        System.out.println(formatter.format(zoned)); // needs zoned date time
        formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
        System.out.println(formatter.format(zoned)); // needs zoned date time
        formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
        System.out.println(formatter.format(dateTime));
        formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
        System.out.println(formatter.format(dateTime));
    }
}
