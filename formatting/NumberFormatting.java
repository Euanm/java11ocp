package formatting;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class NumberFormatting {

    public static void main(String... args) throws ParseException {

        NumberFormat fr = NumberFormat.getInstance(Locale.FRENCH);
        NumberFormat ca = NumberFormat.getInstance(Locale.CANADA_FRENCH);
        NumberFormat uk = NumberFormat.getInstance(Locale.UK);
        NumberFormat us = NumberFormat.getInstance(Locale.US);

        System.out.println("*** Formatting ***");
        float number = 4_980_120.56f;
        System.out.println(fr.format(number));
        System.out.println(ca.format(number));
        System.out.println(uk.format(number));
        System.out.println(us.format(number));

        System.out.println("*** Parsing ***");
        String str = "392.46";

        System.out.println(fr.parse(str));
        System.out.println(ca.parse(str));
        System.out.println(uk.parse(str));
        System.out.println(us.parse(str));
    }
}
