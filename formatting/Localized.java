package formatting;

import java.text.NumberFormat;
import java.util.Locale;

public class Localized {

    public static void main(String[] args) {
        // Creation
        Locale de = Locale.GERMAN;
        Locale de_DE = Locale.GERMANY;
        Locale fr = new Locale("fr");
        Locale es_ES = new Locale("es", "ES");
        Locale us = new Locale.Builder()
                .setLanguage("en")
                .setRegion("US")
                .build();

        // Default Locale
        Locale.setDefault(de_DE);
        print();
        Locale.setDefault(Locale.Category.DISPLAY, us);
        print();
        Locale.setDefault(Locale.Category.FORMAT, us);
        print();
    }

    private static void print() {
        double amount = 5.39;
        System.out.println("---");
        System.out.println(Locale.getDefault().getDisplayLanguage());
        System.out.println(Locale.getDefault().getDisplayName());
        System.out.println(NumberFormat.getCurrencyInstance().format(amount));
    }
}
