package formatting;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class CustomNumberFormatting {

    public static void main(String[] args) {
        double doub = 87654321.345;

        NumberFormat custom01 = new DecimalFormat("###,###,###.00");
        System.out.println(custom01.format(doub));

        NumberFormat custom02 = new DecimalFormat("000,000,000.0000");
        System.out.println(custom02.format(doub));

        NumberFormat custom03 = new DecimalFormat("€ 00,000,000.0000");
        System.out.println(custom03.format(doub));
    }
}
