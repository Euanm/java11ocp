package formatting;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class LocalizedNumber {
    private static Locale us = Locale.US;
    private static Locale fr = Locale.FRANCE;


    public static void main(String[] args) throws ParseException {
        Locale.setDefault(fr);
        System.out.println("Default locale: "+ Locale.getDefault().getDisplayName());
        System.out.println("Custom locale: "+ us.getDisplayName());
        System.out.println();

        System.out.println("*** Number ***");
        double number = 78_436_120.542;
        NumberFormat numberFormatDefault = NumberFormat.getInstance();
        System.out.println("fr: "+ numberFormatDefault.format(number));
        NumberFormat numberFormatUs = NumberFormat.getNumberInstance(us); // getNumberInstance() eq to getInstance()
        System.out.println("us: "+ numberFormatUs.format(number));
        System.out.println();

        System.out.println("*** Currency ***");
        double amount = 46.13;
        NumberFormat currencyFormatDefault = NumberFormat.getCurrencyInstance();
        System.out.println("fr: "+ currencyFormatDefault.format(amount));
        NumberFormat currencyFormatUs = NumberFormat.getCurrencyInstance(us);
        System.out.println("us: "+ currencyFormatUs.format(amount));
        String price = "$19,456.68";
        System.out.println("parse currency: "+ currencyFormatUs.parse(price));
        System.out.println();

        System.out.println("*** percentage ***");
        double percentage = 0.473;
        NumberFormat percentageFormatDefault = NumberFormat.getPercentInstance();
        System.out.println("fr: "+percentageFormatDefault.format(percentage));
        NumberFormat percentageFormatUs = NumberFormat.getPercentInstance(us);
        System.out.println("us: "+ percentageFormatUs.format(percentage));
        String ratio = "19.4%";
        System.out.println("parse percentage us: "+ percentageFormatUs.parse(ratio));
        //System.out.println("parse percentage: "+ percentageFormatDefault.parse(ratio)); //KO
    }
}
