package naming;

public class Var {

    public static void main(String... args) {
        String var = "hello"; // var is not a keyword
        System.out.println(var);
        // var toto = null; null type inference KO
        // var i = 0, j; multiple declaration KO
        float tax_rate =0.06F;
        var price = 100;
        var adjusted_tax = price * tax_rate;
        // price = price + adjusted_tax; price type is int, result is a float
    }
}
