package generic;

class X {}
class Y extends X {}
class Z extends Y {}

public class Confusing<Y> {
    // X x = new Y(); => Y represents the generic type
    // Y y = new Z();
}
