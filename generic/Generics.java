package generic;

public class Generics<T> {
    T t;

    public Generics(T t) { this.t = t; }

    public String toString() {
        return t.toString();
    }

    /**
     * Méthode avec type générique T
     * (même nom que le type générique de la classe)
     *
     * @param message
     * @param <T>
     */
    private <T> void println(T message) { // T n'est pas le même que <T> défini pour la classe
        System.out.println(t + "-" + message);
    }

    public static void main(String[] args) {
        new Generics<String>("hi").println(1);
        new Generics<>("hola").println(true);

        new Generics<String>("ho").<Integer>println(1); // String <T> Classe / Integer <T> méthode
        new Generics<>("halo").<Boolean>println(true); // Boolean <T> méthode
    }
}
