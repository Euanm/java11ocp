package collections;

import java.util.*;

public class Checking {

    public static void main(String[] args) {
        System.out.println("*** Set ***");
        Set<String> set = new HashSet<>();
        set.add("one");
        set.add(null);
        System.out.println(set);
        // Construction d'une liste à partir d'un set
        List fromSet = List.copyOf(set);
        List cstSet = new ArrayList(set);
        // Type
        Set setOf = Set.of("elephant", "girafe", "hippo");
        for(Object str: setOf) {} // set of Object type
        Set<?> wildSetOf = Set.of("elephant", "girafe", "hippo");
        for(Object str: wildSetOf) {} // set of Object type
        Set<? extends String> wildSetOfStrings = Set.of("elephant", "girafe", "hippo");
        for(String str: wildSetOfStrings) {} // set of String type

        System.out.println("*** LinkedList ***");
        LinkedList<String> list = new LinkedList<>();
        list.add("one");
        list.add(null);
        System.out.println(list.get(0)); // récupération par index
        System.out.println("pop(): "+ list.pop());
        System.out.println(list);

        System.out.println("*** Queue ***");
        Queue<String> queue = list;
        queue.offer("three");
        queue.add("four");
        System.out.println(queue);
        queue.poll();
        queue.remove();
        System.out.println(queue);
        System.out.println("peek(): "+ queue.peek());
        System.out.println("element(): "+ queue.element());
        System.out.println("poll(): "+ queue.poll());
        System.out.println(queue.remove());

        System.out.println("*** Map ***");
        Map<String, Double> map = new HashMap<>();
        map.put("float", (double) 99L);
        // Construction d'une liste à partir des paires d'une map
        var map2 = Map.of(1, 2, 3, 4);
        List<Map.Entry> entries = List.copyOf(map2.entrySet());
    }
}
