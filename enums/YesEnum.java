package enums;

public enum YesEnum {
    TOTO("toto"), TATA("tata") {
        void printLibelle() { System.out.println("hello T@t@"); }
    }, TITI("titi");

    private String libelle;

    // public not allowed here
    YesEnum(String libelle) {
        this.libelle = libelle;
    }

    void printLibelle() {
        System.out.println(String.format("Libellé: %s", libelle));
    }

    public static void main(String[] args) {
        YesEnum yesEnum = YesEnum.TITI;
        yesEnum.printLibelle();

        YesEnum tata = YesEnum.TATA;
        tata.printLibelle();
    }
}
