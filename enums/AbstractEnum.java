package enums;

public enum AbstractEnum {
    ONE {
        private String str = "ONE";

        public void myFunc(String str) {
            System.out.println(str);
        }
        public String getMyStr() {
            return str;
        }
    },
    TWO {
        private String str = "TWO";

        void myFunc(String str) {
            System.out.println(str);
        }

        String getMyStr() {
            return str;
        }
    },
    THREE {
        private String myStr = "THREE";

        void myFunc(String str) {
            System.out.println(str);
        }

        public String getMyStr() {
            return myStr;
        }
    };

    private AbstractEnum() {

    }

    abstract void myFunc(String str);
    abstract String getMyStr();

    public static void main (String[] args) {
        AbstractEnum ae = AbstractEnum.THREE;
        ae.myFunc(ae.getMyStr());

        switch(ae) {
            case ONE:
                System.out.println("ONE switch :" + ae.ordinal() );
                break;
            case TWO:
                System.out.println("TWO switch :" + ae.ordinal());
                break;
            default:
                System.out.println("THREE switch :" + ae.ordinal());
        }
    }
}
