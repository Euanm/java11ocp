package methodRef;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class MethodRef {

    private static class Inner {
        private static int longueur = 1;
        private static boolean isCond(String str) {
            return str.length() > longueur;
        }

        private void sayHello() {
            System.out.println("Hello");
        }
    }

    public static void main(String... args) {
        String str = "hello";

        // instance method on parameter
        Predicate<String> blank = String::isBlank;
        System.out.println("1: "+ blank.test(str));

        // Instance method on particular object
        Predicate<String> starts = str::startsWith;
        System.out.println("2: " + starts.test("he"));

        // Static method
        Predicate <String> innerStatic = Inner::isCond;
        System.out.println("3: " + innerStatic.test(str));

        // Constructor
        Supplier<Inner> supply = Inner::new;
        supply.get().sayHello();
    }
}
